console.log("Hello World!");

// Global Object
// Arrays
let students = ["Tony", "Peter", "Wanda", "Vision", "Loki"];
let num = "2";
console.log(students);

/*
	What is the difference between .splice and .slice methods?
		.splice() - removes an element from a specified index and adds elements (Mutator method)

		.slice() - copies a portion of the array from the starting index (Non-mutator method)
	
	What is the other type of array methods?
		iterator methods - loop over the items of an array

	forEach() - loops over items in an array and repeats a user-defined function
	map() - loops over items in an array and repeats a user-defined function AND returns a new array
	every() - loops and checks if all items satisfy a given condition
*/
let arrNum = [15, 20, 25, 30, 11];
/*
	Miniactivity
		using forEach method, check if every item in the array is divisible by 5.
			if they are, log in the console "<num> is divisible by 5"
			if not, log false
		6:45 pm; kindly send your output screenshot in our Google Chat
*/

arrNum.forEach((num) => {
  if (num % 5 === 0) {
    console.log(`${num} is divisible by 5.`);
  } else {
    console.log(false);
  }
});
/*
	can forEach() alone return data that will tell us if ALL numbers/items in the array is divisible by 5?
*/
let divisibleBy5 = arrNum.every((num) => {
  console.log(num);
  return num % 5 === 0;
});

console.log(divisibleBy5);

// Math
// mathematical constants
// 8 pre-defined properties which can be call via the syntax "Math.property" (properties are case-sensitive)
console.log(Math);
console.log(Math.E); //Euler's number
console.log(Math.PI); //PI
console.log(Math.SQRT2); //square root of 2
console.log(Math.SQRT1_2); // square root of 1/2
console.log(Math.LN2); // natural logarithm of 2
console.log(Math.LN10); // natural logarithm of 10
console.log(Math.LOG2E); // base 2 of logarithm of E
console.log(Math.LOG10E); // base 10 of logarithm of E

// methods for rounding a number to an integer
console.log(Math.round(Math.PI)); // rounds to a nearest integer
console.log(Math.ceil(Math.PI)); // rounds UP to a nearest integer
console.log(Math.floor(Math.PI)); // rounds DOWN to nearest integer
console.log(Math.trunc(Math.PI)); //returns only the integer part (ES6 update)

let numVar = -100;

// returns a square root of a number
console.log(Math.sqrt(3.14));
// lowest value in a list of arguments (can accept variables as long as they have number data type as values)
console.log(Math.min(-1, -2, -4, 0, 1, 2, 3, 4, -3, numVar));
//highest values in a list of arguments (can accept variables as long as they have number data type as values)
console.log(Math.max(-1, -2, -4, 0, 1, 2, 3, 4, -3, numVar));

// ========
// activity
// ========
// QUIZ
/*
1 How do you create arrays in JS?

    let array = [];

2 How do you access the first character of an array?

    by using index 0

3 How do you access the last character of an array?

    by using array.length-1

4 What array method searches for, and returns the index of a given value in an array? 
This method returns -1 if given value is not found in the array.

    indexOf()

5 What array method loops over all elements of an array, performing a user-defined function on each iteration?

    forEach()

6.   What array method creates a new array with elements obtained from a user-defined function?

    map()

7.   What array method checks if all its elements satisfy a given condition?

    every()

8.   What array method checks if at least one of its elements satisfies a given condition?

    some()

9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.

    false

10.   True or False: array.slice() copies elements from original array and returns these as a new array.

    true

*/

// FUNCTION CODING
// Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

function addToEnd(arr, data) {
  if (typeof data === "string") {
    arr.push(data);
  } else {
    console.log("error - can only add strings to an array");
  }
  return arr;
}

// Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

function addToStart(arr, data) {
  if (typeof data === "string") {
    arr.unshift(data);
  } else {
    console.log("error - can only add strings to an array");
  }
  return arr;
}

// Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

function elementChecker(arr, data) {
  if (arr.length !== 0) {
    let checker = arr.some((e) => e === data);
    return checker;
  } else {
    return console.log("error - passed in array is empty");
  }
}

/*
	Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:

		if array is empty, return "error - array must NOT be empty"
		if at least one array element is NOT a string, return "error - all array elements must be strings"
		if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/

function checkAllStringsEnding(arr, data) {
  if (arr.length !== 0) {
    let checker = arr.every((e) => typeof e === "string");
    if (checker === true) {
      if (typeof data === "string") {
        if (data.length === 1) {
          let cherker2 = arr.every((e) => e[e.length - 1] === data);
          return cherker2;
        } else {
          return "error - 2nd argument must be a single character";
        }
      } else {
        return "error - 2nd argument must be of data type string";
      }
    } else {
      return "error - all array elements must be strings";
    }
  } else {
    return "error - array must NOT be empty";
  }
}

// Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
function stringLengthSorter(arr) {
  let checker = arr.every((e) => typeof e === "string");
  if (checker === true) {
    let sort = arr.sort(function (a, b) {
      return a.length - b.length;
    });
    return sort;
  } else {
    return "error - all array elements must be strings";
  }
}

/*
	Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:

		if array is empty, return "error - array must NOT be empty"
		if at least one array element is NOT a string, return "error - all array elements must be strings"
		if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		return the number of elements in the array that start with the character argument, must be case-insensitive

	Use the students array and the character "J" as arguments when testing.
*/

function startsWithCounter(arr, data) {
  let count = 0;
  if (arr.length !== 0) {
    let checker = arr.every((e) => typeof e === "string");
    if (checker === true) {
      if (typeof data === "string") {
        if (data.length === 1) {
          arr.forEach((e) => {
            if (e[0].toLowerCase() === data) {
              count++;
            }
          });
          return count;
        } else {
          return "error - 2nd argument must be a single character";
        }
      } else {
        return "error - 2nd argument must be of data type string";
      }
    } else {
      return "error - all array elements must be strings";
    }
  } else {
    return "error - array must NOT be empty";
  }
}

/*
	Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:

	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

	Use the students array and the string "jo" as arguments when testing.
*/

function likeFinder(arr, data) {
  let newarr = [];
  if (arr.length !== 0) {
    let checker = arr.every((e) => typeof e === "string");
    if (checker === true) {
      if (typeof data === "string") {
        arr.forEach((e) => {
          if (e.toLowerCase().includes(data)) {
            newarr.push(e);
          }
        });
        return newarr;
      } else {
        return "error - 2nd argument must be of data type string";
      }
    } else {
      return "error - all array elements must be strings";
    }
  } else {
    return "error - array must NOT be empty";
  }
}

// Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

function randomPicker(arr) {
  let result = arr[Math.floor(Math.random() * arr.length)];
  return result;
}
